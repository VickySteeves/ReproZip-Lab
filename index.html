<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>ReproZip</title>

		<link rel="stylesheet" href="reveal/css/reveal.css">
		<link rel="stylesheet" href="reveal/css/theme/simple.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="reveal/lib/css/zenburn.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'reveal/css/print/pdf.css' : 'reveal/css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h2>Reproducibility with ReproZip</h2><br/>
					
					<p><span style="color:#ff5252;">Vicky Steeves</span>, <span style="color:#ff5252;">Fernando Chirigati</span></p>
					<p>Presentation: <a href="https://vickysteeves.gitlab.io/ReproZip-Lab">vickysteeves.gitlab.io/ReproZip-Lab</a></p>
   				<p>Big Data | April 23, 2018</p>

					<div class='footer'>
						<hr/>
						This work is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</p>
					</div>
				</section>
				
				<section>
					<h2>You just learned a whole lot about provenance and computational reproducibility.</h2>
					<h3><span style="color:	#57068c;">So what if I told you that you could put code + data + analysis + environment in one small file?</span></h3>
					<p>This means ONE file to share with each paper to ensure full reproducibility, verifibility, and authenticity of your research!</p>
				</section>
				
				<section>
					<h2>Tool to Help: ReproZip!</h2>
					
					<p>Here comes ReproZip, the reproducibility packer! ReproZip is a tool aimed at simplifying the process of creating reproducible experiments.</p>
					
					<img src="imgs/reprozip-desc.png" align="middle">

					<aside class="notes">
						So here comes ReproZip to help!  ReproZip is a tool that automatically captures provenance of experiments and packs all the necessary ﬁles, library dependencies, and variables to reproduce the results. Reviewers can then unpack and run the experiments without having to install any additional software. 
					</aside>
				</section>
				
				<section>
    <h2>ReproZip tries to solve...</h2>
        <p style="text-align:left;"><span style="color:#4ba173;">Workload &amp; Time Challenges</span><br/>It is a time commitment to get data and code ready to share, and to share it</p>

        <p>Otherwise known as...</p>

        <p style="text-align:left"><span style="color:#ff5252;">the Incentive Problem</span><br/>Reproducibility takes time, and is not always valued by the academic reward structure</p>

        <span class="fragment fade-in">
            <p style="float: left; width:50%; border: 1px black solid;"><strong>"Insufficient time is the main reason why scientists do not make their data and experiment available and reproducible."</strong><br/>Carol Tenopir, Beyond the PDF2 Conference</p>
            <p style="float: right; width:48%; border: 1px black solid;"><strong>"77% claim that they do not have time to document and clean up the code."</strong><br/>Victoria Stodden, Survey of the Machine Learning Community – NIPS 2010</p>
        </span>

</section>

<section>
    <h2>ReproZip tries to solve...</h2>
        <p style="text-align:left;"><span style="color:#4ba173;">Technical Obsolescence</span><br/>Technology changes affect the reproducibility</p>
        <p style="text-align:left;"><span style="color:#4ba173;">Normative Dissonance<sup>1</sup></span><br/>Espoused values don’t always match practice</p>

        <p>Otherwise known as...</p>

        <p style="text-align:left"><span style="color:#ff5252;">the Pipeline Problem</span><br/>Reproducibility requires skills that are not included in most curriculums!</p>

        <p class="fragment fade-in" style="border: 1px black solid;"><strong>"It would require huge amount of effort to make our code work with the latest versions of these tools."</strong><br/>Collberg et al., Repeatability and Benefaction in Computer Systems Research, University of Arizona TR 14-04</p>

        <p style="font-size:small;"><sup>1</sup><a href="https://www.ncbi.nlm.nih.gov/pubmed/19385804">https://www.ncbi.nlm.nih.gov/pubmed/19385804</a></p>
</section>
								
				<section>
					<h2>2 Steps to Reproducibility: Conceptually</h2>
					<img src="imgs/reprozip-overview.png" align="middle">
					
					<!--SPEAKER NOTES-->
					<aside class="notes">
						This graphic just shows some of the explicit steps involved in making and unpacking a reproducible package. The left shows the first step, packing. Here the original researcher uses reprozip trace, runs their experiment, and at the conclusion simply packs it into the single package. They can then send that package to colleagues or reviewers, or store it as an archival snapshot.

						In the unpacking step, the reviewer or collaborator would load in the rpz, optionally upload their own input files to the environment, and reproduce the experiment. They have the option to download the output files for further inspection, and then destroy the VM or container.

						While ReproZip is written in python, it packs experiments and environments regardless of language by systematically tracking and keeping a record of programs, libraries, and file accesses

						From there it creates a single reproducible package, a .rpz file from that captured information.  

						Others can then unpack the rpz file using ReproUnzip on another machine regardless of configuration or OS. During the unpacking step, users have easy access to various unpackers, including vagrant and docker, to make reproduction simpler. 

						I also wanted to mention that reprozip has the ability to pack graphical tools,  client-server environments, databases,  and interactive experiments. 

						Automatically and systematically captures the provenance of an existing experiment (Linux only) 
							Language-independent approach and solution

						Creates a self-contained reproducible package from captured provenance

						Extracts package in another environment, independent of the operating system

						Provides easy-to-use interfaces for replicating and varying the original configuration of the experiment

						Packs graphical tools, interactive environments, databases, client-server set-ups
					</aside>
				</section>
				
				<section>
					<h2>2 Steps to Reproducibility: Practically</h2>
					<img src="imgs/reprozip-eco.png" align="middle">
				</section>
				
				<section>
					<h2>Step 1: Trace & Pack</h2>
					
<pre><code>reprozip trace [command]</pre></code>
					
					<img src="imgs/reprozip.png" align="middle"  style="margion-bottom:0px; padding-bottom:0px;">
					
<pre><code>reprozip pack package-name.rpz</pre></code>
					
					<p>Before you pack, you can edit the config.yml (but it's not recommended). <a href="https://gitlab.com/VickySteeves/2017-IASSIST-ReproZip/blob/master/bechdel-config.yml">This</a> is what a config looks like.</p>

					<!--SPEAKER NOTES-->
					<aside class="notes">
						I'll show this a bit more explicitly. Right now, Reprozip can only pack experiments and environments from Linux. So we start with this computational environment.  Then the researchers runs the experiment under reprozip trace. Reprozip then traces and records the execution of the experiment. 

						We use some heuristics (a set of rules that normally work) for detecting files:
							Input Files: files that are only read, do not belong to any software package and are not executable + files mentioned on command line
							Log Files: files that are read and written afterwards (logs, database files, etc.)
							Output files: files that are only written to
						
						Reprozip then creates a configuration file that lists the files to be packed so the researcher can add or remove files that they know need to be packed or not. He should be wary of privacy and copyright issues when going over the list. 
						All the files are packed in the same structure they are found in the original environment. The output is a single reproducible rpz file.
					</aside>
				</section>
				
				<section>
					<h2>Step 1: Tracing</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/gs4s2JH8Yw4" frameborder="0" allowfullscreen></iframe>
				</section>

				<section>
					<h2>Step 1b: Packing</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/Qit9sYVHsnY" frameborder="0" allowfullscreen></iframe>
				</section>		

				<section>
					<h2>Step 2: Set up & Run</h2>
					
					<p>Double click on the RPZ file, and choose your unpacker!</p>
					
					<img src="imgs/reproUnzip.png" align="middle">
					
					<!--SPEAKER NOTES-->
					<aside class="notes">
						Now that we have an rpz file, we can unpack it in any OS.
						
						Then we can run the unpacker we like. There are four unpackers currently available.

						The first is the directory unpacker (reprounzip directory) allows users to unpack the entire experiment (including library dependencies) in a single directory, and to reproduce the experiment directly from that directory. It does so by automatically altering environment variables (e.g.: PATH, HOME, and LD_LIBRARY_PATH) and the command line to point the experiment execution to the created directory, which has the same structure as in the packing environment. This is unreliable if the application cannot be trusted, since it can point outside the unpacked directory. Hardcoded paths for example will still hit outside that directory.

						The next is the chroot unpacker (reprounzip chroot), similar to reprounzip directory, a directory is created from the experiment package; however, a full system environment is also built, which can then be run with chroot(2), a Linux mechanism that changes the root directory / for the experiment to the experiment directory. Therefore, this unpacker addresses the limitation of the directory unpacker and does not fail in the presence of hardcoded absolute paths. Note as well that it does not interfere with the current environment since the experiment is isolated in that single directory. Although chroot offers pretty good isolation, it is not considered completely safe: malicious experiments might still escape to the host environment.

						Third, the vagrant unpacker (reprounzip vagrant) allows an experiment to be unpacked and reproduced using a virtual machine created through Vagrant. Therefore, the experiment can be reproduced in any environment supported by this tool, i.e., Linux, Mac OS X, and Windows.

						Lastly, ReproUnzip can extract and reproduce experiments as Docker containers. The docker unpacker (reprounzip docker) is responsible for such integration. Docker implements a high-level API to provide lightweight containers that run processes in isolation. A Docker container, as opposed to a traditional virtual machine, does not require or include a separate operating system. Instead, it relies on the kernel's functionality and uses resource isolation (CPU, memory, block I/O, network, etc.) and separate namespaces to isolate the application's view of the operating system, and is thus lighter and faster.

						ReproZip also allows users to generate a provenance graph related to the experiment execution by reading the metadata available in the .rpz package. This graph shows the experiment runs as well as the files and other dependencies they access during execution; this is particularly useful to visualize and understand the dataflow of the experiment.

						There is also a VisTrails plugin that creates a VisTrails workflow, that can be used to run the experiment in VisTrails.
					</aside>
				</section>
					
				<section>
					<h2>Setting Up</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/YwLipnvvEQA" frameborder="0" allowfullscreen></iframe>
				</section>
				
				<section>
					<h2>(Re)Running</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/uRBDYLuBfjM" frameborder="0" allowfullscreen></iframe>
				</section>
				
				<section>
					<h2>Not just simple reproduction...</h2>
				 
					<p>When you unpack your .rpz package with the GUI, you'll come to this: <img src="imgs/reprounzip-gui.png" width="50%" height="50%"></p>
					
				</section>
				
				<section>
					<h2>Download the results or add your own input!</h2><br/>
					<div style="float: left; width:45%;">
					<h3>Download Ouput</h3>
					<img src="imgs/reprounzip-gui2.png">
					</div>
					
					<div style="float: right; width:45%;">
					<h3>Upload New Inputs</h3>
						<img src="imgs/reprounzip-gui1.png">				
					</div>
				</section>
				
				<section>
					<h2>Optional Step:<br/> Downloading Outputs</h2>
					<iframe class="stretch"src="https://www.youtube.com/embed/ebuOcY4THVM" frameborder="0" allowfullscreen></iframe>
				</section>

				<section>
					<h2>Optional Step:<br/> Uploading Inputs</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/G919J2fptE0" frameborder="0" allowfullscreen></iframe>
				</section>

				<section>
					<h2>Bonus Demo Time:<br/> Jupyter Notebooks</h2>
					
					<p>Jupyter notebooks are wonderful tools, but still subject to a lot of the technical problems we outlined for computational reproducibility. So, we made a plugin to help!</p>
					
					<code><pre>
pip install reprozip-jupyter
jupyter nbextension install --py reprozip_jupyter --user
jupyter nbextension enable --py reprozip_jupyter --user
jupyter serverextension enable --py reprozip_jupyter --user
</code></pre>			
				</section>	
				
				<section>
				<h2>Packing Jupyter Notebooks</h2>
    		<iframe class="stretch" src="https://www.youtube.com/embed/Y8YmGVYHhS8?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
				</section>
				
				<section>
					<h2>Unpacking Jupyter Notebooks</h2>
					<iframe class="stretch" src="https://www.youtube.com/embed/dfHefVD1-Fk" frameborder="0" allowfullscreen></iframe>
				</section>	

				<section>
					<h2>Available provenance graphs</h2>
					<p>There is a static graph available that is built via:</p>
<code><pre>
reprounzip graph graphfile.dot mypackfile.rpz
</code></pre>
					<p>However, the output is HUGE! So you can pare it down with <a href="https://docs.reprozip.org/en/1.0.x/graph.html#common-recipes">flags</a> like <code>--otherfiles io</code> (will show only the input and output files). Then take it from a DOT (a graph description language) file to a .png like so: <code>dot -Tpng graphfile.dot -o graph.png</code></p>
					<img src="imgs/prov-graph.png">
				</section>	
				
				<section>
					<h2>Available provenance graphs</h2>
					<p>And the new interactive graph built on D3, soon to be integrated!</p>
					<img src="imgs/prov-improved.png">
				</section>

				<section>
					<div style="float: left; width:49%;">
						<h3>ReproZip can pack:</h3>
						<div style="border: 1px #ff5252 solid;">
						<p style="margin-top:0px;"><span style="color:#4ba173;">Data analysis scripts / software</span> (any language, you name it!)</p>

						<p style="color:#4ba173;">Graphical tools</p>

						<p style="color:#4ba173;">Interactive tools</p>

						<p><span style="color:#4ba173;">Client-server applications</span> (including databases)</p>

						<p style="color:#4ba173;">Jupyter notebooks</p>

						<p><span style="color:#4ba173;">MPI experiments</span> (setting up the experiment is involved though…)</p>

						<p><strong>… and much more!</strong></p></div>
					</div>
					
					<div style="float: right; width:49%;">
						<h3>Current use cases:</h3>
						<div style="border: 1px #4ba173 solid;">
						<p style="margin-top:0px;">Rec. by the <a href="http://www.journals.elsevier.com/information-systems/" style="color:#ff5252;">Information Systems Journal</a>, Reproducibility Section</p>
						<p>Rec. by the <a href="http://db-reproducibility.seas.harvard.edu/" style="color:#ff5252;">ACM SIGMOD Reproducibility Review</a></p>
						<p>Listed on the ACM <a href="http://www.artifact-eval.org/guidelines.html" style="color:#ff5252;">Artifact Evaluation Process Guidelines</a></p>
						<p>Integrated as a component of <a href="https://github.com/usnistgov/corr-reprozip" style="color:#ff5252;">CoRR</a></p>
						<p>Archiving data journalism apps like <a href="https://github.com/ViDA-NYU/reprozip-examples/tree/master/stacked-up" style="color:#ff5252;">StackedUp</a></p>
						<p><strong>… and many more!</strong></p></div>
					</div>
				</section>
				
				<!-- LAB -->
				<section>
					<h2>Lab Time</h2>
					<p>Let's test out reproducibility in the wild!</p>
				</section>
				
				<section>
					<h2>Part 1: Trying to reproduce other's work without ReproZip</h2>
					<ul>
						<li>Download <a href="https://raw.githubusercontent.com/ViDA-NYU/reprozip-examples/master/digits-sklearn-opencv/generateClassifier.py">generateClassifier.py</a> and <a href="https://raw.githubusercontent.com/ViDA-NYU/reprozip-examples/master/digits-sklearn-opencv/performRecognition.py">performRecognition.py</a>.</li>
						<li>Download <a href="https://github.com/ViDA-NYU/reprozip-examples/raw/master/digits-sklearn-opencv/photo.jpg">input data (one photo)</a>.</li>
						<li>Try to rerun the analysis!</li>
					</ul>
					
					<p>You should generate something like this:</p>
					<img src="imgs/output.jpg">
					
					<p>Time yourselves to see how long it takes to reproduce it!</p>
				</section>
				
				<section>
					<h2>Part 2: Trying to reproduce other's work with ReproZip</h2>
					<ul>
						<li>Make sure you have ReproUnzip (<a href="">Mac</a>, <a href="">Windows</a>, <a href="">Anaconda</a>) and <a href="">Vagrant</a>/<a href="">VirtualBox</a> installed.</li>
						<li>Download the <a href="https://osf.io/5ztp2/">digits_sklearn_opencv.rpz</a>.</li>
					</ul>
					
					<p>Either use the graphical user interface (double click on the .rpz file) or use the terminal to reproduce the experiment from the .rpz file with these commands:</p>			<code><pre>
$ reprounzip vagrant setup digitRecognition.rpz digit-recognition/
$ reprounzip vagrant run digit-recognition/ classification
$ reprounzip vagrant run digit-recognition/ prediction
</code></pre>

				<p>If you have time left, go to the next slide!</p>
				</section>
				
				<section>
					<h2>BONUS: Downloading the output & uploading new input</h2>
					
					<p>After you successfully unpack and rerun the anaylsis, you should be able to download the output file either in the 'Manage files' panel of the GUI or via this command on the terminal:</p><code><pre>$ reprounzip vagrant download digit-recognition/ output.jpg</code></pre>

					<p>You can also try uploading a new input file to test the script on other data! We have a <a href="https://github.com/ViDA-NYU/reprozip-examples/blob/master/digits-sklearn-opencv/photo_2.jpg">new input</a> ready for you. Download it and either upload via the 'Manage files' panel on the GUI or with the following commands in the terminal:<code><pre>$ reprounzip vagrant upload digit-recognition/ photo_2.jpg:photo.jpg
$ reprounzip vagrant run digit-recognition/ prediction
$ reprounzip vagrant download digit-recognition/ output.jpg
</code></pre>
				</section>
				
			</div>
		</div>

		<script src="reveal/lib/js/head.min.js"></script>
		<script src="reveal/js/reveal.js"></script>

		<script>
			// More info https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				history: true,
							// More info https://github.com/hakimel/reveal.js#dependencies
				dependencies: [
					{ src: 'reveal/plugin/markdown/marked.js' },
					{ src: 'reveal/plugin/markdown/markdown.js' },
					{ src: 'reveal/plugin/notes/notes.js', async: true },
					{ src: 'reveal/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
				]
			});
		</script>
	</body>
</html>
